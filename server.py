from flask import Flask, Response, json, request
import pymongo
from bson.objectid  import ObjectId

app=Flask(__name__)


try:
    mongo = pymongo.MongoClient( host="localhost", port = 27017)

    db = mongo.infos

except Exception as e:
    print(e)
    print("Error- Couldnot connect to db")

@app.route('/')
def index():
    return 'Hello'


@app.route('/Info', methods=["POST"])
def create_user():
    try:
        user= { "name": request.form["name"], 
               "lastname":request.form["lastname"]
               }

        dbResponse = db.users.insert_one(user)
       
        return Response(
            response = json.dumps({ "msg":"User Created"}),
            status=200,
            mimetype="application/json"

                        )

    except Exception as ex:
        print(ex)
        return Response( response= json.dumps({ "msg":"Didnot create user"}),
        status=500,
        mimetype="application/json"
        )

@app.route('/Info', methods=["GET"])
def retrieve_users():
    try:
        user_info=list(db.users.find())
        # print(user_info)
        for information in user_info:
            information["_id"]= str(information["_id"])
        return Response( 
            response = json.dumps(user_info),
            status=200,
            mimetype="application/json"
        )
        
    except Exception as e:
        print (e)
        return Response( response= json.dumps({ "msg":"couldnot retrieve user info"}),
        status=500,
        mimetype="application/json"
        )
    
@app.route('/Info/<id>', methods=["PATCH"])
def update_users(id):
    try:
        dbResponse = db.users.update_one(
            {"_id":ObjectId(id)},
            {"$set":{"name":request.form["name"],"lastname":request.form["lastname"]}}
          )
        return Response( 
            response = json.dumps({"msg": "User has been update"}),
            status=200,
            mimetype="application/json"
        )

    except Exception as e:
        print (e)
        return Response( response= json.dumps({ "msg":"couldnot update user info"}),
        status=500,
        mimetype="application/json"
        )    
@app.route('/Info/<id>', methods=["DELETE"])
def delete_users(id):
    try:
        dbResponse = db.users.delete_one({ "_id": ObjectId(id) } )

        return Response( 
            response = json.dumps({"msg": "User has been deleted"}),
            status=200,
            mimetype="application/json"
        )

    except Exception as e:
        print (e)

        return Response( 
            response = json.dumps({"msg": "User didnot get deleted"}),
            status=500,
            mimetype="application/json"
        )


if __name__=='__main__':
    app.run(debug=True)